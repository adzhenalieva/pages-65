import axios from 'axios';

const instance = axios.create({

    baseURL: 'https://burger-template-280989.firebaseio.com/'

});

export default instance;