import React, {Fragment} from 'react';
import Toolbar from "../Toolbar/Toolbar";
import Footer from "../Footer/Footer";

const Layout = ({children}) => {
    return (
        <div>
            <Fragment>
                <Toolbar/>
                <main className="Layout-Content">{children}</main>
                <Footer/>
            </Fragment>
        </div>
    );
};

export default Layout;