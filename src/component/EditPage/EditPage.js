import React, {Component} from 'react';
import {PAGES} from "../../constants";
import axios from '../../axios-pages';
import Spinner from "../UI/Spinner/Spinner";
import './EditPage.css';

class EditPage extends Component {

    state = {
        page: null,
        id: PAGES[0],
        loading: true
    };

    loadData = (id) => {
        axios.get('pages/' + id + '.json').then(response => {
            this.setState({page: response.data, loading: false});
        })
    };

    componentDidMount() {
        this.loadData(this.state.id);
    }

    valueChanged = event => {
        let page = {...this.state.page};
        let name = event.target.name;
        page[name] = event.target.value;
        this.setState({page})
    };

    changeId = event => {
        this.setState({id: event.target.value});
        this.loadData(event.target.value);
    };

    editPage = (event, page) => {
        event.preventDefault();
        axios.put('pages/' + this.state.id + '.json', page).then(() => {
            this.props.history.push({
                pathname: '/pages/' + this.state.id
            });
        })
    };

    render() {
        return (
            this.state.loading ?
                <Spinner/> :
                <form className="EditForm" onSubmit={event => this.editPage(event, this.state.page)}>
                    <select name="id"
                            onChange={this.changeId}
                            value={this.state.id}>
                        {PAGES.map(id => (
                                <option key={id} value={id}>{id}</option>
                            )
                        )}
                    </select>
                    <input type="text" name="title"
                              onChange={this.valueChanged}
                              value={this.state.page.title}
                    />
                    <textarea name="content"
                           onChange={this.valueChanged}
                           value={this.state.page.content} rows={15}
                    />
                    <input type="text" name="image"
                           onChange={this.valueChanged}
                           value={this.state.page.image}
                    />
                    <button type="submit">Save</button>
                </form>
        );
    }
}

export default EditPage;