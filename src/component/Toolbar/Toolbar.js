import React from 'react';
import {NavLink} from "react-router-dom";
import './Toolbar.css';

const Toolbar = () => {
    return (
            <nav className="MainNav">
                <img className="Logo" src={"https://cdn.iconscout.com/icon/free/png-256/lego-47-569262.png"} alt="lego"/>
                <NavLink className="NavLink" activeClassName="Active" to="/" exact={true} >Home</NavLink>
                <NavLink className="NavLink" activeClassName="Active"  to="/pages/about" >About</NavLink>
                <NavLink className="NavLink" activeClassName="Active" to="/pages/superheroes">Superheroes</NavLink>
                <NavLink className="NavLink" activeClassName="Active" to="/pages/portfolio">Portfolio</NavLink>
                <NavLink className="NavLink" activeClassName="Active" to="/pages/contacts">Contacts</NavLink>
                <NavLink className="NavLink" activeClassName="Active" to="/pages/admin">Admin</NavLink>
            </nav>
    );
};

export default Toolbar;