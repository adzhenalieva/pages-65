import React, {Component} from 'react';
import axios from '../axios-pages';
import Spinner from "../component/UI/Spinner/Spinner";
import './MainPage.css';

class MainPage extends Component {

    state = {
        page: [],
        loading: true
    };

    loadData() {
        let url = 'pages.json';
        const id = this.props.match.params.id;
        if (id) {
            url += `?orderBy="id"&equalTo="${id}"`
        } else{
            url += `?orderBy="id"&equalTo="main"`
        }
        axios.get(url).then(response => {
            const page = Object.keys(response.data).map(name => {
                return {...response.data[name]}
            });
            this.setState({loading: false, page});
        })

    }

    componentDidMount() {
        this.loadData();
    }

    componentDidUpdate(prevProps) {
        if (this.props.match.params.id !== prevProps.match.params.id) {
            this.loadData();
        }
    }

    render() {
        return (
            this.state.loading ?
                <Spinner/> :
                <div className="MainPage">
                    <h2>{this.state.page[0].title}</h2>
                    <p>{this.state.page[0].content}</p>
                    <img src={this.state.page[0].image} alt="source"/>
                </div>
        );
    }
}

export default MainPage;