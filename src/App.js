import React, {Component} from 'react';
import {BrowserRouter, Switch, Route} from "react-router-dom";

import './App.css';
import MainPage from "./container/MainPage";
import Layout from "./component/Layout/Layout";
import EditPage from "./component/EditPage/EditPage";

class App extends Component {
    render() {
        return (
            <div className="App">
                <BrowserRouter>
                    <Layout>
                        <Switch>
                            <Route path="/" exact component={MainPage}/>
                            <Route path="/pages/admin" component={EditPage}/>
                            <Route path="/pages/:id" component={MainPage}/>
                        </Switch>
                    </Layout>
                </BrowserRouter>
            </div>
        );
    }
}

export default App;
